/*
	水墨画っぽいアウトラインを引くシェーダー by あるる（きのもと 結衣）
	Water color-esque outline shader by @arlez80

	MIT License
*/

shader_type spatial;
render_mode cull_front, depth_draw_never, unshaded;

uniform float thickness = 0.015;
uniform float noise = 0.6;
uniform vec4 color : hint_color = vec4( 0.0, 0.0, 0.0, 1.0 );

varying vec3 origin_vertex;

void vertex( )
{
	VERTEX += NORMAL * thickness;
	origin_vertex = VERTEX;
}

float random( vec3 pos )
{ 
	return fract(sin(dot(pos, vec3(12.9898,78.233,53.532532))) * 43758.5453);
}

float value_noise( vec3 pos )
{
	vec3 p = floor( pos );
	vec3 f = fract( pos );

	float v000 = random( p/*+ vec3( 0.0, 0.0, 0.0 )*/ );
	float v100 = random( p + vec3( 1.0, 0.0, 0.0 ) );
	float v010 = random( p + vec3( 0.0, 1.0, 0.0 ) );
	float v110 = random( p + vec3( 1.0, 1.0, 0.0 ) );
	float v001 = random( p + vec3( 0.0, 0.0, 1.0 ) );
	float v101 = random( p + vec3( 1.0, 0.0, 1.0 ) );
	float v011 = random( p + vec3( 0.0, 1.0, 1.0 ) );
	float v111 = random( p + vec3( 1.0, 1.0, 1.0 ) );

	vec3 u = f * f * ( 3.0 - 2.0 * f );

	return mix(
		mix(
			mix( v000, v100, u.x )
		,	mix( v010, v110, u.x )
		,	u.y
		)
	,	mix(
			mix( v001, v101, u.x )
		,	mix( v011, v111, u.x )
		,	u.y
		)
	,	u.z
	);
}

void fragment( )
{
	vec3 v = origin_vertex;
	float f = clamp( ( (
		value_noise( v * 1.84643 ) * 0.5
	+	value_noise( v * 5.45432 ) * 0.25
	+	value_noise( v * 15.754824 ) * 0.125
	+	value_noise( v * 35.4274729 ) * 0.0625
	+	value_noise( v * 95.65347829 ) * 0.03125
	+	value_noise( v * 285.528934 ) * 0.015625
	+	value_noise( v * 585.495328 ) * 0.0078125
	+	value_noise( v * 880.553426553 ) * 0.00390625
	) - 0.25 ) * 4.0, 0.0, 1.0 );
	ALBEDO = color.rgb;
	ALPHA = mix( 1.0, f, noise );
}
