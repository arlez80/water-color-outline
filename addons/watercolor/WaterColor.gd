tool
extends Node

class_name WaterColorEsqueOutline

export(float, 0.0, 1.0) var outline_thickness = 0.035 setget set_outline_thickness
export(float, 0.0, 1.0) var outline_noise = 0.6 setget set_outline_noise
export(Color) var outline_color = Color( 0, 0, 0, 1 ) setget set_outline_color

var shader_material:ShaderMaterial

func _ready( ):
	self._set_all_mesh_instances( )

func set_outline_thickness( _outline_thickness:float ):
	outline_thickness = _outline_thickness
	self._set_all_mesh_instances( )

func set_outline_noise( _outline_noise:float ):
	outline_noise = _outline_noise
	self._set_all_mesh_instances( )

func set_outline_color( _outline_color:Color ):
	outline_color = _outline_color
	self._set_all_mesh_instances( )

func _set_all_mesh_instances( ):
	if self.shader_material == null:
		self.shader_material = ShaderMaterial.new( )
		self.shader_material.shader = preload( "outline.shader" )
	self.shader_material.set_shader_param( "thickness", self.outline_thickness )
	self.shader_material.set_shader_param( "noise", self.outline_noise )
	self.shader_material.set_shader_param( "color", self.outline_color )
	self._search_mesh_instances( self )

func _search_mesh_instances( node:Node ):
	if node is MeshInstance:
		var mi:MeshInstance = node
		if mi.material_override != null:
			print( "override", mi.material_override )
			self._apply_water_color_outline_shader( mi.material_override )
		else:
			for i in range( mi.get_surface_material_count( ) ):
				self._apply_water_color_outline_shader( mi.get_surface_material( i ) )
			for i in range( mi.mesh.get_surface_count( ) ):
				self._apply_water_color_outline_shader( mi.mesh.surface_get_material( i ) )

	for c in node.get_children( ):
		self._search_mesh_instances( c )

func _apply_water_color_outline_shader( m:Material ):
	if m == null:
		return

	while true:
		if m == self.shader_material:
			break
		if m.next_pass == null:
			m.next_pass = self.shader_material
			break
		if m.next_pass is ShaderMaterial and m.next_pass.shader == self.shader_material.shader:
			m.next_pass = self.shader_material
			break
		m = m.next_pass
