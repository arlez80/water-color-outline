tool
extends EditorPlugin

"""
	Water Color-esque outline shader
"""

func _enter_tree():
	self.add_custom_type( "WaterColorEsqueOutline", "Spatial", preload("WaterColor.gd"), preload("icon.png") )

func _exit_tree():
	pass
